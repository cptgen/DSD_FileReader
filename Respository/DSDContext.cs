﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DSD_FileReader.Respository
{
    public class DSDContext : DbContext
    {
        public DbSet<DSDReceive> DSDReceive { get; set; }
        public DbSet<Urea> Urea { get; set; }
        public DbSet<burner> burner { get; set; }
        public DSDContext(DbContextOptions<DSDContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DSDReceive>()
                .Property(b => b.READ_YN)
                .HasDefaultValue("N");
            base.OnModelCreating(modelBuilder);
        }
    }
}
