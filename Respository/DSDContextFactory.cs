﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSD_FileReader.Respository
{
    public class DSDContextFactory : IDesignTimeDbContextFactory<DSDContext>
    {
        public DSDContext CreateDbContext(string[] args)
        {
            var serverVersion = new MySqlServerVersion(new Version(5, 7, 34));
          
            var options = new DbContextOptionsBuilder<DSDContext>()
             .UseMySql("Server=49.50.164.241;Database=DSD;Port=2705; Uid=dsd_user; Pwd=dsd3853#@;CharSet=utf8", serverVersion).Options;
            return new DSDContext(options);
        }
    }
}
