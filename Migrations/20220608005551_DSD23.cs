﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DSD_FileReader.Migrations
{
    public partial class DSD23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EVENTTIME",
                table: "TB_UREA",
                newName: "DSD_EVENT_TIME");

            migrationBuilder.RenameColumn(
                name: "EVENTDATE",
                table: "TB_UREA",
                newName: "DSD_EVENT_DATE");

            migrationBuilder.RenameColumn(
                name: "EVENTTIME",
                table: "TB_BURNER",
                newName: "DSD_EVENT_TIME");

            migrationBuilder.RenameColumn(
                name: "EVENTDATE",
                table: "TB_BURNER",
                newName: "DSD_EVENT_DATE");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DSD_EVENT_TIME",
                table: "TB_UREA",
                newName: "EVENTTIME");

            migrationBuilder.RenameColumn(
                name: "DSD_EVENT_DATE",
                table: "TB_UREA",
                newName: "EVENTDATE");

            migrationBuilder.RenameColumn(
                name: "DSD_EVENT_TIME",
                table: "TB_BURNER",
                newName: "EVENTTIME");

            migrationBuilder.RenameColumn(
                name: "DSD_EVENT_DATE",
                table: "TB_BURNER",
                newName: "EVENTDATE");
        }
    }
}
