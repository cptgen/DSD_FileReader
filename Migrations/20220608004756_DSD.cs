﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DSD_FileReader.Migrations
{
    public partial class DSD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TB_BURNER",
                columns: table => new
                {
                    BURN_SEQ = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SOFTWARE_VER = table.Column<short>(type: "smallint", nullable: false),
                    STATE = table.Column<short>(type: "smallint", nullable: false),
                    DPF_regen_flag = table.Column<short>(type: "smallint", nullable: false),
                    BLDC_SPD_PID_CTRL = table.Column<short>(type: "smallint", nullable: false),
                    ID_TC1 = table.Column<short>(type: "smallint", nullable: false),
                    ID_EGT1 = table.Column<short>(type: "smallint", nullable: false),
                    ID_EGT2 = table.Column<short>(type: "smallint", nullable: false),
                    ID_EGT3 = table.Column<short>(type: "smallint", nullable: false),
                    ID_T_PCB = table.Column<short>(type: "smallint", nullable: false),
                    ID_P1 = table.Column<short>(type: "smallint", nullable: false),
                    ID_P2 = table.Column<short>(type: "smallint", nullable: false),
                    ID_P3 = table.Column<short>(type: "smallint", nullable: false),
                    ID_P4 = table.Column<short>(type: "smallint", nullable: false),
                    ID_T1 = table.Column<short>(type: "smallint", nullable: false),
                    ID_T2 = table.Column<short>(type: "smallint", nullable: false),
                    ID_T3 = table.Column<short>(type: "smallint", nullable: false),
                    ID_T4 = table.Column<short>(type: "smallint", nullable: false),
                    ID_VTG_BAT = table.Column<short>(type: "smallint", nullable: false),
                    ID_PWR_CUR = table.Column<short>(type: "smallint", nullable: false),
                    I_HEATER1 = table.Column<short>(type: "smallint", nullable: false),
                    HTR1_ENABLE = table.Column<short>(type: "smallint", nullable: false),
                    BLDC_ENABLE = table.Column<short>(type: "smallint", nullable: false),
                    SOL1_ENABLE = table.Column<short>(type: "smallint", nullable: false),
                    BLDC_SPD_CMD = table.Column<short>(type: "smallint", nullable: false),
                    HTR1_DUTY_AUTO = table.Column<short>(type: "smallint", nullable: false),
                    BURNER_TEMP_ERR_FLAG = table.Column<short>(type: "smallint", nullable: false),
                    BLDC_ERR_FLAG = table.Column<short>(type: "smallint", nullable: false),
                    GP_ERR_FLAG = table.Column<short>(type: "smallint", nullable: false),
                    REC_SEQ = table.Column<int>(type: "int", nullable: false),
                    EVENT_DATE = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    DSD_NAME = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DSD_DATA_TYPE = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EVENTDATE = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EVENTTIME = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TB_BURNER", x => x.BURN_SEQ);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "TB_UREA",
                columns: table => new
                {
                    UREA_SEQ = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TIME_SYS_HOUR = table.Column<short>(type: "smallint", nullable: false),
                    TIME_SYSTEM_MIN = table.Column<short>(type: "smallint", nullable: false),
                    POWER_SW = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    STATE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    NSR_MODE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    UWS_AMT_FDBACK_MODE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    NH3_STORAGE_MODE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SCR_AGING_MODE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    ID_TS_UP = table.Column<short>(type: "smallint", nullable: false),
                    ID_TS_DN = table.Column<short>(type: "smallint", nullable: false),
                    ID_P_UREA = table.Column<short>(type: "smallint", nullable: false),
                    ID_TS_AMB = table.Column<short>(type: "smallint", nullable: false),
                    ID_T_PCB = table.Column<short>(type: "smallint", nullable: false),
                    ID_VTG_BAT = table.Column<short>(type: "smallint", nullable: false),
                    ID_SYS_CUR = table.Column<short>(type: "smallint", nullable: false),
                    NOX_UP = table.Column<short>(type: "smallint", nullable: false),
                    NOX_DN = table.Column<short>(type: "smallint", nullable: false),
                    O2_UP = table.Column<short>(type: "smallint", nullable: false),
                    O2_DN = table.Column<short>(type: "smallint", nullable: false),
                    NO2_DN = table.Column<short>(type: "smallint", nullable: false),
                    NO_DN = table.Column<short>(type: "smallint", nullable: false),
                    NH3_DN = table.Column<short>(type: "smallint", nullable: false),
                    LEVEL_TANK_PCNT = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    UQS_PCNT = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    UQS_TEMP = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    T_TANK = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SV_dev100 = table.Column<short>(type: "smallint", nullable: false),
                    NSR_factor = table.Column<short>(type: "smallint", nullable: false),
                    SCR_AGING_factor = table.Column<short>(type: "smallint", nullable: false),
                    DOSING_VALVE_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    PRPUMP_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    BFPUMP_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SM_HTR_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    HTUBE_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    NOX_ENABLE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    PRPUMP_ONTIME = table.Column<short>(type: "smallint", nullable: false),
                    PRPUMP_OFFTIME = table.Column<short>(type: "smallint", nullable: false),
                    BFPUMP_ONTIME = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    BFPUMP_OFFTIME = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    DOSING_DUTY = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SM_HTR_DUTY = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    HTUBE_DUTY = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    DOSING_FREQ = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SM_HTR_FREQ = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    HTUBE_FREQ = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    FLAG_DOSING_P_CON = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    FLAG_DOSING_T_CON1 = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    FLAG_DOSING_T_CON2 = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    ALPHA_ratio_cmd_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    ALPHA_ratio_act_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    EXMASS_PHY = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Init_BASE_Cur_mA = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Init_PRPUMP_Cur_mA = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Init_BFPUMP_Cur_mA = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    Init_INJECTOR_Cur_mA = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    UWS_STOICH_mg_min = table.Column<short>(type: "smallint", nullable: false),
                    UWS_DEMAND_mg_min = table.Column<short>(type: "smallint", nullable: false),
                    i_THETA = table.Column<short>(type: "smallint", nullable: false),
                    i_THETA_ads_rate_m10000 = table.Column<short>(type: "smallint", nullable: false),
                    i_THETA_des_rate_m10000 = table.Column<short>(type: "smallint", nullable: false),
                    NH3_ads_ctl_max_mol_m3 = table.Column<short>(type: "smallint", nullable: false),
                    NH3_inSCR_act_mg_m10 = table.Column<short>(type: "smallint", nullable: false),
                    NH3_RLEASE_mg_min = table.Column<short>(type: "smallint", nullable: false),
                    AVG_EXPT_NOX_EFFI_DOS_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    AVG_ACT_NOX_EFFI_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    AVG_ACT_NOX_EFFI_DOS_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    RATIO_NOX_EFFI_ERR_pcnt = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    AVG_UWS_INJ_mL = table.Column<short>(type: "smallint", nullable: false),
                    UWS_CONSUMP_TANK_mL = table.Column<short>(type: "smallint", nullable: false),
                    RATIO_UWS_CONSUMP_ERR = table.Column<short>(type: "smallint", nullable: false),
                    FLAG_ACT_CHECK_AT_START = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    NONE = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    FLAG_NO_DIAGNOSIS = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    ERR_SCR_SYS = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                    SENS_ERR_CODE = table.Column<short>(type: "smallint", nullable: false),
                    ACT_ERR_CODE = table.Column<short>(type: "smallint", nullable: false),
                    SYS_ERR_CODE = table.Column<short>(type: "smallint", nullable: false),
                    REC_SEQ = table.Column<int>(type: "int", nullable: false),
                    EVENT_DATE = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    DSD_NAME = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    DSD_DATA_TYPE = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EVENTDATE = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    EVENTTIME = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TB_UREA", x => x.UREA_SEQ);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TB_BURNER");

            migrationBuilder.DropTable(
                name: "TB_UREA");
        }
    }
}
