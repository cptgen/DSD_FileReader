﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSD_FileReader.Respository
{

    [DelimitedRecord(",")]
    public class UreaReader
    {
       
        public string DSD_EVENT_DATE { get; set; }
        
        public string DSD_EVENT_TIME { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 TIME_SYS_HOUR { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 TIME_SYSTEM_MIN { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte POWER_SW { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte STATE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte NSR_MODE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte UWS_AMT_FDBACK_MODE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte NH3_STORAGE_MODE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte SCR_AGING_MODE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_TS_UP { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_TS_DN { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_P_UREA { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_TS_AMB { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T_PCB { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_VTG_BAT { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_SYS_CUR { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NOX_UP { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NOX_DN { get; set; }

        [FieldConverter(ConverterKind.Int16)]        
        public Int16 O2_UP { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 O2_DN { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NO2_DN { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NO_DN { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NH3_DN { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte LEVEL_TANK_PCNT { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte UQS_PCNT { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte UQS_TEMP { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte T_TANK { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SV_dev100 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NSR_factor { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SCR_AGING_factor { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte DOSING_VALVE_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte PRPUMP_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte BFPUMP_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte SM_HTR_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte HTUBE_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte NOX_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 PRPUMP_ONTIME { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 PRPUMP_OFFTIME { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte BFPUMP_ONTIME { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte BFPUMP_OFFTIME { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte DOSING_DUTY { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte SM_HTR_DUTY { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte HTUBE_DUTY { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte DOSING_FREQ { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte SM_HTR_FREQ { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte HTUBE_FREQ { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte FLAG_DOSING_P_CON { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte FLAG_DOSING_T_CON1 { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte FLAG_DOSING_T_CON2 { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte ALPHA_ratio_cmd_pcnt { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte ALPHA_ratio_act_pcnt { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte EXMASS_PHY { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte Init_BASE_Cur_mA { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte Init_PRPUMP_Cur_mA { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte Init_BFPUMP_Cur_mA { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte Init_INJECTOR_Cur_mA { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 UWS_STOICH_mg_min { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 UWS_DEMAND_mg_min { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 i_THETA { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 i_THETA_ads_rate_m10000 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 i_THETA_des_rate_m10000 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NH3_ads_ctl_max_mol_m3 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NH3_inSCR_act_mg_m10 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 NH3_RLEASE_mg_min { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte AVG_EXPT_NOX_EFFI_DOS_pcnt { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte AVG_ACT_NOX_EFFI_pcnt { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte AVG_ACT_NOX_EFFI_DOS_pcnt { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte RATIO_NOX_EFFI_ERR_pcnt { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 AVG_UWS_INJ_mL { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 UWS_CONSUMP_TANK_mL { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 RATIO_UWS_CONSUMP_ERR { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte FLAG_ACT_CHECK_AT_START { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte NONE { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte FLAG_NO_DIAGNOSIS { get; set; }

        [FieldConverter(ConverterKind.Byte)]
        public Byte ERR_SCR_SYS { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SENS_ERR_CODE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ACT_ERR_CODE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SYS_ERR_CODE { get; set; }
    }


    [DelimitedRecord(",")]
    public class BurnerReader
    {
        public string DSD_EVENT_DATE { get; set; }

        public string DSD_EVENT_TIME { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SOFTWARE_VER { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 STATE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 DPF_regen_flag { get; set; }

        [FieldConverter(ConverterKind.Int16)]        
        public Int16 BLDC_SPD_PID_CTRL { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_TC1 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_EGT1 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_EGT2 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_EGT3 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T_PCB { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_P1 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_P2 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_P3 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_P4 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T1 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T2 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T3 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_T4 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_VTG_BAT { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 ID_PWR_CUR { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 I_HEATER1 { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 HTR1_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 BLDC_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 SOL1_ENABLE { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 BLDC_SPD_CMD { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 HTR1_DUTY_AUTO { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 BURNER_TEMP_ERR_FLAG { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 BLDC_ERR_FLAG { get; set; }

        [FieldConverter(ConverterKind.Int16)]
        public Int16 GP_ERR_FLAG { get; set; }
    }


        [Table("TB_RECEIVE")]
    public class DSDReceive
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int REC_SEQ { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        public string DATA_TYPE { get; set; }

        public int DSD_NO { get; set; }

        [MaxLength(800)]
        public string RECEIVE_DATA { get; set; }

        [DefaultValue("N")]
        [Column(TypeName = "char")]
        [StringLength(1)]        
        public string READ_YN { get; set; }

        [DefaultValue("CURRENT_TIMESTAMP")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime EVENT_DATE { get; set; }
    }

    
    public class BaseType
    {
        [Description("DSD IDENTIFIER NAME")]
        
        public int DSD_NO { get; set; }

        [Description("UREA / BURNER / GPS")]
        [Column(TypeName = "char")]
        [StringLength(3)]
        public string DSD_DATA_TYPE { get; set; }
        [StringLength(10)]
        public string DSD_EVENT_DATE { get; set; }
        [StringLength(10)]
        public string DSD_EVENT_TIME { get; set; }
    }
    [Table("TB_UREA")]
    public class Urea : BaseType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UREA_SEQ { get; set; }
        public Int16 TIME_SYS_HOUR { get; set; }
        public Int16 TIME_SYSTEM_MIN { get; set; }
        public Byte POWER_SW { get; set; }
        public Byte STATE { get; set; }
        public Byte NSR_MODE { get; set; }
        public Byte UWS_AMT_FDBACK_MODE { get; set; }
        public Byte NH3_STORAGE_MODE { get; set; }
        public Byte SCR_AGING_MODE { get; set; }
        public Int16 ID_TS_UP { get; set; }
        public Int16 ID_TS_DN { get; set; }
        public Int16 ID_P_UREA { get; set; }
        public Int16 ID_TS_AMB { get; set; }
        public Int16 ID_T_PCB { get; set; }
        public Int16 ID_VTG_BAT { get; set; }
        public Int16 ID_SYS_CUR { get; set; }
        public Int16 NOX_UP { get; set; }
        public Int16 NOX_DN { get; set; }
        public Int16 O2_UP { get; set; }
        public Int16 O2_DN { get; set; }
        public Int16 NO2_DN { get; set; }
        public Int16 NO_DN { get; set; }
        public Int16 NH3_DN { get; set; }
        public Byte LEVEL_TANK_PCNT { get; set; }
        public Byte UQS_PCNT { get; set; }
        public Byte UQS_TEMP { get; set; }
        public Byte T_TANK { get; set; }
        public Int16 SV_dev100 { get; set; }
        public Int16 NSR_factor { get; set; }
        public Int16 SCR_AGING_factor { get; set; }
        public Byte DOSING_VALVE_ENABLE { get; set; }
        public Byte PRPUMP_ENABLE { get; set; }
        public Byte BFPUMP_ENABLE { get; set; }
        public Byte SM_HTR_ENABLE { get; set; }
        public Byte HTUBE_ENABLE { get; set; }
        public Byte NOX_ENABLE { get; set; }
        public Int16 PRPUMP_ONTIME { get; set; }
        public Int16 PRPUMP_OFFTIME { get; set; }
        public Byte BFPUMP_ONTIME { get; set; }
        public Byte BFPUMP_OFFTIME { get; set; }
        public Byte DOSING_DUTY { get; set; }
        public Byte SM_HTR_DUTY { get; set; }
        public Byte HTUBE_DUTY { get; set; }
        public Byte DOSING_FREQ { get; set; }
        public Byte SM_HTR_FREQ { get; set; }
        public Byte HTUBE_FREQ { get; set; }
        public Byte FLAG_DOSING_P_CON { get; set; }
        public Byte FLAG_DOSING_T_CON1 { get; set; }
        public Byte FLAG_DOSING_T_CON2 { get; set; }
        public Byte ALPHA_ratio_cmd_pcnt { get; set; }
        public Byte ALPHA_ratio_act_pcnt { get; set; }
        public Byte EXMASS_PHY { get; set; }
        public Byte Init_BASE_Cur_mA { get; set; }
        public Byte Init_PRPUMP_Cur_mA { get; set; }
        public Byte Init_BFPUMP_Cur_mA { get; set; }
        public Byte Init_INJECTOR_Cur_mA { get; set; }
        public Int16 UWS_STOICH_mg_min { get; set; }
        public Int16 UWS_DEMAND_mg_min { get; set; }
        public Int16 i_THETA { get; set; }
        public Int16 i_THETA_ads_rate_m10000 { get; set; }
        public Int16 i_THETA_des_rate_m10000 { get; set; }
        public Int16 NH3_ads_ctl_max_mol_m3 { get; set; }
        public Int16 NH3_inSCR_act_mg_m10 { get; set; }
        public Int16 NH3_RLEASE_mg_min { get; set; }
        public Byte AVG_EXPT_NOX_EFFI_DOS_pcnt { get; set; }
        public Byte AVG_ACT_NOX_EFFI_pcnt { get; set; }
        public Byte AVG_ACT_NOX_EFFI_DOS_pcnt { get; set; }
        public Byte RATIO_NOX_EFFI_ERR_pcnt { get; set; }
        public Int16 AVG_UWS_INJ_mL { get; set; }
        public Int16 UWS_CONSUMP_TANK_mL { get; set; }
        public Int16 RATIO_UWS_CONSUMP_ERR { get; set; }
        public Byte FLAG_ACT_CHECK_AT_START { get; set; }
        public Byte NONE { get; set; }
        public Byte FLAG_NO_DIAGNOSIS { get; set; }
        public Byte ERR_SCR_SYS { get; set; }
        public Int16 SENS_ERR_CODE { get; set; }
        public Int16 ACT_ERR_CODE { get; set; }
        public Int16 SYS_ERR_CODE { get; set; }

        public int REC_SEQ { get; set; }

        [DefaultValue("CURRENT_TIMESTAMP")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime EVENT_DATE { get; set; }
    }
    [Table("TB_BURNER")]
    public class burner :BaseType
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BURN_SEQ { get; set; }
        public Int16 SOFTWARE_VER { get; set; }
        public Int16 STATE { get; set; }
        public Int16 DPF_regen_flag { get; set; }
        public Int16 BLDC_SPD_PID_CTRL { get; set; }
        public Int16 ID_TC1 { get; set; }
        public Int16 ID_EGT1 { get; set; }
        public Int16 ID_EGT2 { get; set; }
        public Int16 ID_EGT3 { get; set; }
        public Int16 ID_T_PCB { get; set; }
        public Int16 ID_P1 { get; set; }
        public Int16 ID_P2 { get; set; }
        public Int16 ID_P3 { get; set; }
        public Int16 ID_P4 { get; set; }
        public Int16 ID_T1 { get; set; }
        public Int16 ID_T2 { get; set; }
        public Int16 ID_T3 { get; set; }
        public Int16 ID_T4 { get; set; }
        public Int16 ID_VTG_BAT { get; set; }
        public Int16 ID_PWR_CUR { get; set; }
        public Int16 I_HEATER1 { get; set; }
        public Int16 HTR1_ENABLE { get; set; }
        public Int16 BLDC_ENABLE { get; set; }
        public Int16 SOL1_ENABLE { get; set; }
        public Int16 BLDC_SPD_CMD { get; set; }
        public Int16 HTR1_DUTY_AUTO { get; set; }
        public Int16 BURNER_TEMP_ERR_FLAG { get; set; }
        public Int16 BLDC_ERR_FLAG { get; set; }
        public Int16 GP_ERR_FLAG { get; set; }

        public int REC_SEQ { get; set; }

        [DefaultValue("CURRENT_TIMESTAMP")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime EVENT_DATE { get; set; }
    }

    public class DSDReceive_DTO
    {
        public string DATA_TYPE { get; set; }
        public string RECEIVE_DATA { get; set; }
        public int DSD_NO { get; set; }
        public int REC_SEQ { get; set; }
        public DateTime EVENT_DATE { get; set; }
    }

}
