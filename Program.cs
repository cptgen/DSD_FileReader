﻿using System;
using System.Configuration;

using System.Collections.Concurrent;
using System.ComponentModel;
using SimpleInjector;
using AutoMapper;
using FileHelpers;
using DSD_FileReader.Respository;
using Microsoft.EntityFrameworkCore;
using SimpleInjector.Lifestyles;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

using System.Threading;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

using Rebus.Bus;
using Rebus.Config;
using Rebus.Handlers;
using Rebus.SimpleInjector;
using Rebus.Transport.InMem;
using Rebus.Activation;

using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using System.Net;
using Newtonsoft.Json;
namespace DSD_FileReader
{

    class Program
    {
        static string _filePath;
        static string _dsd_Connection;
        static BackgroundWorker _fileReaderWorker;
        static BackgroundWorker _dataBaseWorker;
        static BackgroundWorker _rebusPushWorker;
        static BackgroundWorker _kafkaProduceWorker;
        
        static SimpleInjector.Container _container;      
        static ConcurrentQueue<DSDReceive_DTO> _processQueue;
        static ConcurrentQueue<BaseType> _rebusQueue;

        static ConcurrentQueue<BaseType> _kafkaQueue;
        static void Main(string[] args)
        {                           
            var log = new LoggerConfiguration()
                     .MinimumLevel.Verbose()
                     .WriteTo.Console(theme: AnsiConsoleTheme.Literate)
                     //.WriteTo.ColoredConsole()
                     .WriteTo.File("logs/log_.txt"
                                   , rollingInterval: RollingInterval.Day
                                   , rollOnFileSizeLimit: true
                                   , outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level:u3}] {Message:lj}{NewLine}{Exception}"
                                   , retainedFileCountLimit: 31)
                     .CreateLogger();           
            Log.Logger = log;
            EventWaitHandle ewh = new EventWaitHandle(false, EventResetMode.ManualReset);
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            Console.CancelKeyPress += (s, e) =>
            {
                log.Information($"Console.CancelKeyPress() - s:{s.ToString()}, e:{e.ToString()}");
                e.Cancel = true;
                ewh.Set();
            };
            _dsd_Connection = ConfigurationManager.AppSettings.Get("DSD");
            _filePath = ConfigurationManager.AppSettings.Get("FilePath");
            _container = Bootstrap();
            WorkerCreateStart();
            ewh.WaitOne();
        }

        private static void WorkerCreateStart()
        {
            try
            {
                _fileReaderWorker = new BackgroundWorker
                {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };
                
                _dataBaseWorker = new BackgroundWorker
                {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };
                
                _rebusPushWorker = new BackgroundWorker
                {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };
                
                _kafkaProduceWorker = new BackgroundWorker
                {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };

                _processQueue = new ConcurrentQueue<DSDReceive_DTO>();
               // _rebusQueue = new ConcurrentQueue<BaseType>();
                _kafkaQueue = new ConcurrentQueue<BaseType>();

                _fileReaderWorker.DoWork += _fileReaderWorker_DoWork;
                _dataBaseWorker.DoWork += _dataBaseWorker_DoWork;
                //_rebusPushWorker.DoWork += _rebusPushWorker_DoWork;
                _kafkaProduceWorker.DoWork += _kafkaProduceWorker_DoWork;


                _fileReaderWorker.RunWorkerAsync();
                _dataBaseWorker.RunWorkerAsync();
                _kafkaProduceWorker.RunWorkerAsync();
                //_rebusPushWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex.Message);
            }           
        }

        private static void _kafkaProduceWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BaseType baseData = new BaseType();
            Action<DeliveryReport<Null, string>> handler = r =>
            Log.Logger.Information(!r.Error.IsError
            ? $"Delivered message to {r.TopicPartitionOffset}"
            : $"Delivery Error: {r.Error.Reason}");

            string topic = ConfigurationManager.AppSettings.Get("KAFKA_TOPIC");
            ProducerConfig config = new ProducerConfig
            {
                BootstrapServers = ConfigurationManager.AppSettings.Get("KAFKA_BORKER"),
                SaslMechanism = SaslMechanism.Plain,
                SaslUsername = ConfigurationManager.AppSettings.Get("KAFKA_USER_NAME"),
                SaslPassword = ConfigurationManager.AppSettings.Get("KAFKA_PASSWORD"),
                SecurityProtocol = SecurityProtocol.SaslPlaintext,
                ClientId = Dns.GetHostName(),
            };
            
            using (var producer = new ProducerBuilder<Null, string>(config).Build())
            {                
                while (!_kafkaProduceWorker.CancellationPending)
                {
                    while (_kafkaQueue.TryDequeue(out baseData))
                    {
                        try
                        {
                            if (baseData is Urea)
                            {                               
                                var urea = (Urea)baseData;
                                producer.Produce(topic,
                                               new Message<Null, string> { Value = JsonConvert.SerializeObject(urea) }
                                               , handler);
                            }
                            else
                            {
                                var burner = (burner)baseData;
                                producer.Produce(topic,
                                              new Message<Null, string> { Value = JsonConvert.SerializeObject(burner) }
                                              , handler);

                                
                            }
                            producer.Flush(TimeSpan.FromMinutes(1));
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex.Message);
                        }
                    }
                }                
            }

        }

        private async static void _rebusPushWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BaseType baseData = new BaseType();
            using (var activator = new BuiltinHandlerActivator())
            {
                var bus = Configure.With(activator)
                    .Logging(l => l.ColoredConsole(minLevel: Rebus.Logging.LogLevel.Warn))
                     //.Logging(l => l.Serilog(Log.Logger))                                                                   
                     .Transport(t => t.UsePostgreSqlAsOneWayClient(connectionString: "Server=hobbangs.duckdns.org;Database=iot;Port=3306; Uid=root; Pwd=smcs1009;CharSet=utf8; ", tableName: "rebusMessage"))
                     .Subscriptions(s => s.StoreInMySql(connectionString: "Server=hobbangs.duckdns.org;Database=iot;Port=3306; Uid=root; Pwd=smcs1009;CharSet=utf8; ", tableName: "rebus", automaticallyCreateTables: true, isCentralized: true))
                    .Start();
                while (!_dataBaseWorker.CancellationPending)
                {
                    while (_rebusQueue.TryDequeue(out baseData))
                    {
                        try
                        {
                            if (baseData is Urea)
                            {
                                //a//wait bus.Publish();
                                var urea = (Urea)baseData;
                                await bus.Publish(new EventMessage<Urea>(urea));
                            }
                            else
                            {
                                var burner = (burner)baseData;
                                await bus.Publish(new StringMessage("burnner!"));
                            }
                        }
                        catch (Exception ex)
                        {

                            Log.Logger.Error(ex.Message);
                        }
                        
                        System.Threading.Thread.Sleep(200);
                    }
                }               
            }

            
        }

        private async static void _dataBaseWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var dto = new DSDReceive_DTO();
            var configReader = new MapperConfiguration(c => c.CreateMap<UreaReader, Urea>());
            var configBurnner = new MapperConfiguration(c => c.CreateMap<BurnerReader, burner>());
            var readerMapper = new Mapper(configReader);
            var burnerMapper = new Mapper(configBurnner);
            while (!_dataBaseWorker.CancellationPending)
            {
                try
                {
                    while (_processQueue.TryDequeue(out dto))
                    {
                        if (dto.DATA_TYPE == "U-D")
                        {
                            using (AsyncScopedLifestyle.BeginScope(_container))
                            {
                                var engine = _container.GetInstance<FileHelperAsyncEngine<UreaReader>>();
                                using (engine.BeginReadString(dto.RECEIVE_DATA))
                                {
                                    foreach (var reader in engine)
                                    {
                                        var ctx = _container.GetInstance<DSDContext>();
                                        var urea_entity = readerMapper.Map<Urea>(reader);
                                        urea_entity.REC_SEQ = dto.REC_SEQ;
                                        urea_entity.DSD_NO = dto.DSD_NO;
                                        urea_entity.DSD_DATA_TYPE = dto.DATA_TYPE;
                                        ctx.Urea.Add(urea_entity);
                                        await ctx.SaveChangesAsync();

                                        var urea = await ctx.Urea.FirstOrDefaultAsync(x => x.REC_SEQ == dto.REC_SEQ);
                                        //_rebusQueue.Enqueue(urea);
                                        _kafkaQueue.Enqueue(urea);
                                        var receive = await ctx.DSDReceive.FirstOrDefaultAsync(x => x.REC_SEQ == dto.REC_SEQ);
                                        if(receive != null)
                                        {
                                            receive.READ_YN = "Y";
                                            await ctx.SaveChangesAsync();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (AsyncScopedLifestyle.BeginScope(_container))
                            {
                                var engine = _container.GetInstance<FileHelperAsyncEngine<BurnerReader>>();
                                using (engine.BeginReadString(dto.RECEIVE_DATA))
                                {
                                    foreach (var reader in engine)
                                    {
                                        var ctx = _container.GetInstance<DSDContext>();
                                        var burner_entity = burnerMapper.Map<burner>(reader);
                                        burner_entity.REC_SEQ = dto.REC_SEQ;
                                        burner_entity.DSD_NO = dto.DSD_NO;
                                        burner_entity.DSD_DATA_TYPE = dto.DATA_TYPE;
                                        ctx.burner.Add(burner_entity);
                                        await ctx.SaveChangesAsync();
                                        var burner= await ctx.burner.FirstOrDefaultAsync(x => x.REC_SEQ == dto.REC_SEQ);
                                        //_rebusQueue.Enqueue(burner);
                                        _kafkaQueue.Enqueue(burner);
                                        var receive = await ctx.DSDReceive.FirstOrDefaultAsync(x => x.REC_SEQ == dto.REC_SEQ);
                                        if (receive != null)
                                        {
                                            receive.READ_YN = "Y";
                                            await ctx.SaveChangesAsync();
                                        }                                      
                                    }
                                }
                            }                            
                        }
                    }
                }
                catch (Exception ex)
                {

                    Log.Logger.Error(ex.Message);
                }
                System.Threading.Thread.Sleep(100);

            }
        }

        public class EventMessage<T> where T : class 
        {
            public T Message { get; set; }
            public EventMessage(T messge)
            {
                Message = messge;
            }
        }

        public class StringMessage
        {
            public string Text { get; }

            public StringMessage(string text)
            {
                Text = text;
            }
        }

        private async static void _fileReaderWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!_fileReaderWorker.CancellationPending)
            {
                try
                {                    
                    foreach (var file in Directory.GetFiles(_filePath))
                    {                       
                        string[] readLines = await File.ReadAllLinesAsync(file);
                        Log.Logger.Information($"{file} File Reading... Row Count {readLines.Length}");
                        foreach (var line in readLines)
                        {
                            Log.Logger.Information($"{file} File ReadLine Of Line Data {line}");
                            var header = line.Split(',');
                            if (header.Length > 2)
                            {
                                var dsd_no = Convert.ToInt32(header[0]);
                                var data_type = header[1];
                                var dsd_data = new string[header.Length -2];
                                Array.Copy(header, 2 , dsd_data, 0, header.Length - 2);
                                var receiveData = new DSDReceive
                                {
                                    DSD_NO = dsd_no,
                                    DATA_TYPE = data_type,
                                    RECEIVE_DATA = string.Join("," , dsd_data)
                                };
                                await ResultFileAsync(receiveData);
                            }
                        }
                        //string readFileName = $"ReadFile/{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_Read_{Path.GetFileName(file)}" ;

                        File.Delete(file);
                        string readFileName = $"ReadFile/{Path.GetFileName(file)}";
                        await File.WriteAllLinesAsync(readFileName, readLines);
                        
                    }                    
                }
                catch (Exception ex)
                {

                    Log.Logger.Information(ex.Message);
                }
                System.Threading.Thread.Sleep(100);
            };
        }

        public static async Task<int> ResultFileAsync(DSDReceive data)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                var ctx = _container.GetInstance<DSDContext>();
                await ctx.AddAsync(data);
                int result = await ctx.SaveChangesAsync();
                if (data.DATA_TYPE.Substring(data.DATA_TYPE.Length - 1) == "D")
                {
                    if (result > 0)
                    {
                        var lastReceive = await ctx.DSDReceive.OrderByDescending(x=>x.REC_SEQ).FirstOrDefaultAsync();
                        var mapperConfig = _container.GetInstance<MapperConfiguration>();
                        var mapper = new Mapper(mapperConfig);
                        var dsd_receive_dto = mapper.Map<DSDReceive_DTO>(lastReceive);
                        _processQueue.Enqueue(dsd_receive_dto);
                    }
                }                
                return result;
            }
        }

        private static SimpleInjector.Container Bootstrap()
        {
            var container = new SimpleInjector.Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();            
            var serverVersion = new MySqlServerVersion(new Version(5, 7, 34));
            var options = new DbContextOptionsBuilder<DSDContext>()
               .UseMySql(ConfigurationManager.AppSettings.Get("DSD"), serverVersion)
               .LogTo(Log.Information, Microsoft.Extensions.Logging.LogLevel.Information)
               .EnableSensitiveDataLogging()
               .EnableDetailedErrors()
               .Options;
            container.RegisterSingleton<MapperConfiguration>(() => new MapperConfiguration(cfg => cfg.CreateMap<DSDReceive, DSDReceive_DTO>()));
            container.Register<DSDContext>(() => new DSDContext(options),Lifestyle.Scoped);
            container.Register<FileHelperAsyncEngine<UreaReader>>(() => new FileHelperAsyncEngine<UreaReader>(), Lifestyle.Singleton);
            container.Register<FileHelperAsyncEngine<BurnerReader>>(() => new FileHelperAsyncEngine<BurnerReader>(), Lifestyle.Scoped);

            container.Verify();            
            return container;
        }

        class StringHandler : IHandleMessages<string>
        {
            public async Task Handle(string message)
            {
                await Task.Delay(100);
            }
        }
        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            if (_fileReaderWorker.IsBusy)
                _fileReaderWorker.CancelAsync();

            if (_dataBaseWorker.IsBusy)
                _dataBaseWorker.CancelAsync();

            //if (_rebusPushWorker.IsBusy)
            //    _rebusPushWorker.CancelAsync();

            if (_kafkaProduceWorker.IsBusy)
                _kafkaProduceWorker.CancelAsync();

        }
    }
}
