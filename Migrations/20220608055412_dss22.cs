﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DSD_FileReader.Migrations
{
    public partial class dss22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DSD_NAME",
                table: "TB_UREA");

            migrationBuilder.DropColumn(
                name: "DSD_NAME",
                table: "TB_BURNER");

            migrationBuilder.AddColumn<int>(
                name: "DSD_NO",
                table: "TB_UREA",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DSD_NO",
                table: "TB_BURNER",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DSD_NO",
                table: "TB_UREA");

            migrationBuilder.DropColumn(
                name: "DSD_NO",
                table: "TB_BURNER");

            migrationBuilder.AddColumn<string>(
                name: "DSD_NAME",
                table: "TB_UREA",
                type: "varchar(4)",
                maxLength: 4,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "DSD_NAME",
                table: "TB_BURNER",
                type: "varchar(4)",
                maxLength: 4,
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");
        }
    }
}
